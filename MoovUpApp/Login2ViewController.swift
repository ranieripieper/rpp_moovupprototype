//
//  Login2ViewController.swift
//  MoovUpApp
//
//  Created by Gilson Gil on 11/17/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit
import MapKit

class Login2ViewController: UIViewController {
  
  @IBOutlet weak var loginViewControllerImageView: UIImageView!
  @IBOutlet weak var bgView: UIView!
  @IBOutlet weak var bgViewBottomSpaceConstraint: NSLayoutConstraint!
  @IBOutlet weak var googlePlusButtonHorizontalCenterConstraint: NSLayoutConstraint!
  @IBOutlet weak var loginFieldsWidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var loginLabelCenterYConstraint: NSLayoutConstraint!
  @IBOutlet weak var nameTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  @IBOutlet weak var tap: UITapGestureRecognizer!
  
  var loginFieldsHidden = true
  var firstAppear = true
  weak var loginAnimator: LoginAnimator?
  
  override func preferredStatusBarStyle() -> UIStatusBarStyle {
    return .Default
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleKeyboardUp:", name: UIKeyboardWillShowNotification, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleKeyboardDown:", name: UIKeyboardWillHideNotification, object: nil)
    nameTextField.setPaddings()
    passwordTextField.setPaddings()
    nameTextField.setLightPlaceholder("Nome")
    passwordTextField.setLightPlaceholder("Senha")
    loginViewControllerImageView.addGestureRecognizer(tap)
  }
  
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    if !firstAppear {
      googlePlusButtonHorizontalCenterConstraint.constant = 210
      loginFieldsWidthConstraint.constant = 190
    }
    firstAppear = false
    view.layoutIfNeeded()
    googlePlusButtonHorizontalCenterConstraint.constant = 210
    loginFieldsWidthConstraint.constant = 190
    UIView.animateWithDuration(0.3, animations: { () -> Void in
      self.nameTextField.becomeFirstResponder()
      self.view.layoutIfNeeded()
    }) { (finished) -> Void in
      self.loginFieldsHidden = false
    }
  }
  
  @IBAction func didTap(sender: UITapGestureRecognizer) {
    if sender.locationInView(sender.view).y <= 145 {
      navigationController?.popViewControllerAnimated(true)
    }
  }
  
  func handleKeyboardUp(notification: NSNotification) {
    let durationValue = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
    let duration = durationValue.doubleValue
    let frameValue = notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue
    let frame = frameValue.CGRectValue()
    
    bgViewBottomSpaceConstraint.constant = frame.height
    loginLabelCenterYConstraint.constant = -view.bounds.width
    UIView.animateWithDuration(duration, animations: { () -> Void in
      self.view.layoutIfNeeded()
    })
  }
  
  func handleKeyboardDown(notification: NSNotification) {
    let durationValue = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
    let duration = durationValue.doubleValue
    
    bgViewBottomSpaceConstraint.constant = 0
    loginLabelCenterYConstraint.constant = 0
    UIView.animateWithDuration(duration, animations: { () -> Void in
      self.view.layoutIfNeeded()
    })
  }
}

extension Login2ViewController: UITextFieldDelegate {
  
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    if textField == nameTextField {
      passwordTextField.becomeFirstResponder()
    } else {
      textField.resignFirstResponder()
      (UIApplication.sharedApplication().delegate as! AppDelegate).goToNewGoalApplicationFlowFromLogin(true)
    }
    return true
  }
}
