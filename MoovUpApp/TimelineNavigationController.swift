//
//  TimelineNavigationController.swift
//  MoovUpApp
//
//  Created by Gilson Gil on 11/17/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class TimelineNavigationController: UINavigationController {
  
  @IBOutlet weak var menuAnimator: MenuAnimator!
  
  weak var menuViewController: MenuViewController?
  var timelineViewController: TimelineViewController?
  
  override func preferredStatusBarStyle() -> UIStatusBarStyle {
    return .LightContent
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    view.addGestureRecognizer(menuAnimator.menuPanGesture)
  }

  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if let menuViewController = segue.destinationViewController as? MenuViewController {
      menuViewController.transitioningDelegate = menuAnimator
      menuViewController.timelineNavigationController = self
      self.menuViewController = menuViewController
    }
  }
  
  func showTimelineViewController() {
    if timelineViewController == nil {
      return
    }
    viewControllers = [timelineViewController!]
  }
  
  func showAskSpecialistViewController() {
    timelineViewController = viewControllers.first as? TimelineViewController
    let askSpecialistViewController = storyboard!.instantiateViewControllerWithIdentifier("AskSpecialistViewController") as! AskSpecialistViewController
    viewControllers = [askSpecialistViewController]
  }
}
