//
//  AskSpecialistViewController.swift
//  MoovUpApp
//
//  Created by Gilson Gil on 1/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class AskSpecialistViewController: UIViewController {
  
  @IBOutlet weak var textView: UITextView!
  @IBOutlet weak var subjectButton: UIButton!
  @IBOutlet weak var okButton: UIButton!
  @IBOutlet weak var okButtonBottomSpaceConstraint: NSLayoutConstraint!
  @IBOutlet weak var tap: UITapGestureRecognizer!

  weak var pickerView: PickerView?
  let descriptionTextViewPlaceholder = "Descrição"
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleKeyboardUp:", name: UIKeyboardWillShowNotification, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleKeyboardDown:", name: UIKeyboardWillHideNotification, object: nil)
    view.addGestureRecognizer(tap)
  }
  
  override func viewWillDisappear(animated: Bool) {
    super.viewWillDisappear(animated)
    NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
    NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
  }

  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//    if let askSpecialistResponseViewController = segue.destinationViewController as? AskSpecialistResponseViewController {
//      askSpecialistResponseViewController
//    }
    navigationItem.backBarButtonItem = UIBarButtonItem (title: "", style: .Bordered, target: nil, action: nil)
  }
  
  @IBAction func showMenu(sender: UIBarButtonItem) {
    navigationController?.performSegueWithIdentifier("SegueMenu", sender: sender)
  }
  
  @IBAction func okTapped(sender: UIButton) {
    performSegueWithIdentifier("SegueSpecialistsResponse", sender: nil)
  }
  
  @IBAction func tapped(sender: UITapGestureRecognizer) {
    textView.resignFirstResponder()
  }
  
  @IBAction func toggleGoalOptions(sender: UIButton) {
    if pickerView != nil {
      pickerView!.dismiss()
    } else {
      pickerView = NSBundle.mainBundle().loadNibNamed("PickerView", owner: self, options: nil).first as? PickerView
      pickerView!.frame = CGRect(x: 0, y: view.bounds.height, width: view.bounds.width, height: 206)
      view.layoutIfNeeded()
      pickerView!.presentInViewController(self)
    }
  }
  
  func handleKeyboardUp(notification: NSNotification) {
    if let durationValue = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as? NSNumber, let frameValue = notification.userInfo![UIKeyboardFrameEndUserInfoKey] as? NSValue {
      let duration = durationValue.doubleValue
      let frame = frameValue.CGRectValue()
      
      okButtonBottomSpaceConstraint.constant = frame.height - 20 - okButton.frame.height
      UIView.animateWithDuration(duration, animations: { () -> Void in
        self.view.layoutIfNeeded()
      })
    }
  }
  
  func handleKeyboardDown(notification: NSNotification) {
    if let durationValue = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as? NSNumber {
      let duration = durationValue.doubleValue
      
      okButtonBottomSpaceConstraint.constant = 20
      UIView.animateWithDuration(duration, animations: { () -> Void in
        self.view.layoutIfNeeded()
      })
    }
  }
}

extension AskSpecialistViewController: PickerViewDelegate {
  
  func doneWithIndex(index: Int) {
    switch index {
      case 0:
        subjectButton.setTitle("Saúde", forState: .Normal)
      case 1:
        subjectButton.setTitle("Exercício Físico", forState: .Normal)
      case 2:
        subjectButton.setTitle("Conquistas Pessoais", forState: .Normal)
      case 3:
        subjectButton.setTitle("Mente", forState: .Normal)
      case 4:
        subjectButton.setTitle("Viagens", forState: .Normal)
      default:
        subjectButton.setTitle("Tema do assunto", forState: .Normal)
    }
  }
}

extension AskSpecialistViewController: UITextViewDelegate {
  
  func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
    let string = (textView.text as NSString).stringByReplacingCharactersInRange(range, withString: text)
    okButton.enabled = count(string) > 0
    return true
  }
  
  func textViewDidBeginEditing(textView: UITextView) {
    if textView.text == descriptionTextViewPlaceholder {
      textView.text = ""
      textView.textColor = UIColor.blackColor()
    }
  }
  
  func textViewDidEndEditing(textView: UITextView) {
    if count(textView.text) == 0 {
      textView.text = descriptionTextViewPlaceholder
      textView.textColor = UIColor(red: 203.0/255, green: 203.0/255, blue: 203.0/255, alpha: 1.0)
    }
  }
}
