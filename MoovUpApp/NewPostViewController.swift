//
//  NewPostViewController.swift
//  MoovUpApp
//
//  Created by Gilson Gil on 11/25/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class NewPostViewController: UIViewController {
  
  @IBOutlet weak var valueTextField: UITextField!
  @IBOutlet weak var descriptionTextView: UITextView!
  @IBOutlet weak var goalImageView: UIImageView!
  @IBOutlet weak var pictureImageView: UIImageView!
  @IBOutlet weak var okButton: UIButton!
  @IBOutlet weak var tap: UITapGestureRecognizer!
  @IBOutlet weak var pictureImageViewHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var okButtonBottomSpaceConstraint: NSLayoutConstraint!
  
  let descriptionTextViewPlaceholder = "Descrição"
  
  var goal: Goal?

  override func viewDidLoad() {
    super.viewDidLoad()
    valueTextField.setPaddings()
    valueTextField.setBorders()
    descriptionTextView.setPaddings()
    descriptionTextView.setBorders()
    if goal != nil {
      goalImageView.image = UIImage(named: goal!.icon)
      valueTextField.placeholder = goal!.options[goal!.selectedOption!].2
    }
    view.addGestureRecognizer(tap)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleKeyboardUp:", name: UIKeyboardWillShowNotification, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleKeyboardDown:", name: UIKeyboardWillHideNotification, object: nil)
  }
  
  @IBAction func dismiss(sender: UIButton) {
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  @IBAction func changePicture(sender: UIButton) {
    if IOS_8() {
      let imagePicker = UIImagePickerController()
      imagePicker.delegate = self
      if UIImagePickerController.isCameraDeviceAvailable(.Rear) || UIImagePickerController.isCameraDeviceAvailable(.Front) {
        let alert = UIAlertController(title: "Escolha", message: nil, preferredStyle: .ActionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .Default, handler: { (alertAction) -> Void in
          imagePicker.sourceType = .Camera
          self.presentViewController(imagePicker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Biblioteca", style: .Default, handler: { (alertAction) -> Void in
          imagePicker.sourceType = .PhotoLibrary
          self.presentViewController(imagePicker, animated: true, completion: nil)
        }))
        presentViewController(alert, animated: true, completion: nil)
      } else {
        imagePicker.sourceType = .PhotoLibrary
        self.presentViewController(imagePicker, animated: true, completion: nil)
      }
    }
  }
  
  @IBAction func dismissPictureTapped(sender: UIButton) {
    pictureImageViewHeightConstraint.constant = 1
    UIView.animateWithDuration(0.3, animations: { () -> Void in
      self.view.layoutIfNeeded()
    }) { (finished) -> Void in
      self.pictureImageView.image = nil
    }
  }
  
  @IBAction func didTap(sender: UIButton) {
    valueTextField.resignFirstResponder()
    descriptionTextView.resignFirstResponder()
  }
  
  @IBAction func okTapped(sender: UIButton) {
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  func handleKeyboardUp(notification: NSNotification) {
    let durationValue = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
    let duration = durationValue.doubleValue
    let frameValue = notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue
    let frame = frameValue.CGRectValue()
    
    okButtonBottomSpaceConstraint.constant = frame.height - 20 - okButton.frame.height
    UIView.animateWithDuration(duration, animations: { () -> Void in
      self.view.layoutIfNeeded()
    })
  }
  
  func handleKeyboardDown(notification: NSNotification) {
    let durationValue = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
    let duration = durationValue.doubleValue
    
    okButtonBottomSpaceConstraint.constant = 20
    UIView.animateWithDuration(duration, animations: { () -> Void in
      self.view.layoutIfNeeded()
    })
  }
}

extension NewPostViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  
  func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
    let image = info[UIImagePickerControllerOriginalImage] as! UIImage
    self.pictureImageView.image = image
    dismissViewControllerAnimated(true, completion: { () -> Void in
      self.view.layoutIfNeeded()
      self.pictureImageViewHeightConstraint.constant = 150
      UIView.animateWithDuration(0.3, animations: { () -> Void in
        self.view.layoutIfNeeded()
      })
    })
  }
  
  func imagePickerControllerDidCancel(picker: UIImagePickerController) {
    dismissViewControllerAnimated(true, completion: nil)
  }
}

extension NewPostViewController: UITextFieldDelegate {
  
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    descriptionTextView.becomeFirstResponder()
    return true
  }
}

extension NewPostViewController: UITextViewDelegate {
  
  func textViewDidBeginEditing(textView: UITextView) {
    if textView.text == descriptionTextViewPlaceholder {
      textView.text = ""
      textView.textColor = UIColor.blackColor()
    }
  }
  
  func textViewDidEndEditing(textView: UITextView) {
    if count(textView.text) == 0 {
      textView.text = descriptionTextViewPlaceholder
      textView.textColor = UIColor(red: 203.0/255, green: 203.0/255, blue: 203.0/255, alpha: 1.0)
    }
  }
}
