//
//  AskSpecialistResponseViewController.swift
//  MoovUpApp
//
//  Created by Gilson Gil on 1/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class AskSpecialistResponseViewController: UIViewController {
  
  @IBOutlet weak var phone1WidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var phone2WidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var phone3WidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var phone4WidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var phone5WidthConstraint: NSLayoutConstraint!

  @IBAction func clapTapped(sender: UIButton) {
    sender.selected = !sender.selected
  }
  
  @IBAction func phoneTapped(sender: UIButton) {
    sender.selected = !sender.selected
    switch sender.tag {
    case 1:
      phone1WidthConstraint.constant = (sender.selected ? 1 : 0) * 71
    case 2:
      phone2WidthConstraint.constant = (sender.selected ? 1 : 0) * 71
    case 3:
      phone3WidthConstraint.constant = (sender.selected ? 1 : 0) * 71
    case 4:
      phone4WidthConstraint.constant = (sender.selected ? 1 : 0) * 71
    case 5:
      phone5WidthConstraint.constant = (sender.selected ? 1 : 0) * 71
    default:
      break
    }
    UIView.animateWithDuration(0.3, animations: { () -> Void in
      self.view.layoutIfNeeded()
    })
  }
}
