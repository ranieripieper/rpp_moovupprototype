//
//  NewGoalViewController.swift
//  MoovUpApp
//
//  Created by Gilson Gil on 11/15/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class NewGoalViewController: UIViewController {
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if let newGoalDetailViewController = segue.destinationViewController as? NewGoalDetailViewController {
      if let goal = sender as? Goal {
        newGoalDetailViewController.goal = goal
      }
    }
    navigationItem.backBarButtonItem = UIBarButtonItem (title: "", style: .Bordered, target: nil, action: nil)
  }
  
  @IBAction func goalTapped(sender: UIButton) {
    let tag = sender.tag
    let goal = Goal(type: tag)
    performSegueWithIdentifier("SegueNewGoalDetail", sender: goal)
  }
  
  @IBAction func notAGoalTapped(sender: UIButton) {
    let goal = Goal(type: Int(arc4random() % 5) + 1)
    goal.selectedOption = min(Int(Int(arc4random()) % goal.options.count), goal.options.count - 1)
    (UIApplication.sharedApplication().delegate as! AppDelegate).goToTimelineApplicationFlow(goal)
  }
}
