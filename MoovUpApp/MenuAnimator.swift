//
//  MenuAnimator.swift
//  MoovUpApp
//
//  Created by Gilson Gil on 11/17/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class MenuAnimator: UIPercentDrivenInteractiveTransition, UIViewControllerAnimatedTransitioning {
  
  @IBOutlet weak var navigationController: UINavigationController!
  @IBOutlet weak var menuPanGesture: UIPanGestureRecognizer!
  
  var dimView: UIView?
  var presenting = false
  var interactive = false
  
  func transitionDuration(transitionContext: UIViewControllerContextTransitioning) -> NSTimeInterval {
    return 0.3
  }
  
  func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
    if presenting {
      let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!
      let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
      let containerView = transitionContext.containerView()
      
      dimView = UIView(frame: CGRect(x: 0, y: 64, width: containerView.bounds.width, height: containerView.bounds.height))
      dimView!.backgroundColor = UIColor.blackColor()
      dimView!.alpha = 0.0
      containerView.addSubview(dimView!)
      containerView.addSubview(toViewController.view)
      toViewController.view.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, containerView.bounds.width, 0)
      
      UIView.animateWithDuration(transitionDuration(transitionContext), animations: { () -> Void in
        toViewController.view.transform = CGAffineTransformIdentity
        self.dimView!.alpha = 0.7
      }) { (finished) -> Void in
        transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
      }
    } else {
      let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!
      let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
      let containerView = transitionContext.containerView()
      
      UIView.animateWithDuration(transitionDuration(transitionContext), animations: { () -> Void in
        self.dimView?.alpha = 0.0
        fromViewController.view.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, containerView.bounds.width, 0)
      }) { (finished) -> Void in
        if !transitionContext.transitionWasCancelled() {
          self.dimView?.removeFromSuperview()
//        } else {
//          self.dimView?.alpha = 0.7
        }
        transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
      }
    }
  }
  
  @IBAction func menuOpenPanGestureRecognizer(menuPanGestureRecognizer: UIPanGestureRecognizer) {
    let view = self.navigationController.view
    if menuPanGestureRecognizer.state == .Began {
      let location = menuPanGestureRecognizer.locationInView(view)
      if location.x > view.bounds.width * 0.9 {
        interactive = true
        navigationController.performSegueWithIdentifier("SegueMenu", sender: nil)
      }
    } else if menuPanGestureRecognizer.state == .Changed {
      let translation = menuPanGestureRecognizer.translationInView(view)
      let d = fabs(translation.x / CGRectGetWidth(view.bounds))
      self.updateInteractiveTransition(d)
    } else if menuPanGestureRecognizer.state == .Ended {
      if menuPanGestureRecognizer.velocityInView(view).x < 0 {
        self.finishInteractiveTransition()
      } else {
        self.cancelInteractiveTransition()
      }
      interactive = false
    }
  }
}

extension MenuAnimator: UIViewControllerTransitioningDelegate {
  
  func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    self.presenting = true
    return self
  }
  
  func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    self.presenting = false
    return self
  }
  
  func interactionControllerForPresentation(animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
    self.presenting = true
    return interactive ? self : nil
  }
  
  func interactionControllerForDismissal(animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
    self.presenting = false
    return interactive ? self : nil
  }
}
