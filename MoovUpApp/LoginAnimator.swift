//
//  LoginAnimator.swift
//  MoovUpApp
//
//  Created by Gilson Gil on 11/17/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class LoginAnimator: UIPercentDrivenInteractiveTransition, UIViewControllerAnimatedTransitioning {

  var presenting = false
  
  func transitionDuration(transitionContext: UIViewControllerContextTransitioning) -> NSTimeInterval {
    return 0.3
  }
  
  func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
    if presenting {
      let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey) as! LoginViewController
      let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey) as! Login2ViewController
      let containerView = transitionContext.containerView()
      
      UIGraphicsBeginImageContextWithOptions(fromViewController.view.bounds.size, true, 0.0)
      fromViewController.view.drawViewHierarchyInRect(fromViewController.view.bounds, afterScreenUpdates: false)
      toViewController.loginViewControllerImageView.image = UIGraphicsGetImageFromCurrentImageContext()
      UIGraphicsEndImageContext()
      
      containerView.addSubview(toViewController.view)

      transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
    } else {
      let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey) as! Login2ViewController
      let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey) as! LoginViewController
      let containerView = transitionContext.containerView()

      fromViewController.nameTextField.resignFirstResponder()
      fromViewController.passwordTextField.resignFirstResponder()
      fromViewController.loginFieldsWidthConstraint.constant = 0
      fromViewController.googlePlusButtonHorizontalCenterConstraint.constant = 0
      
      UIView.animateWithDuration(transitionDuration(transitionContext), animations: { () -> Void in
        fromViewController.view.layoutIfNeeded()
      }) { (finished) -> Void in
        containerView.insertSubview(toViewController.view, belowSubview: fromViewController.view)
        transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
      }
    }
  }
}

extension LoginAnimator: UIViewControllerTransitioningDelegate {
  
  func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    self.presenting = true
    return self
  }
  
  func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    self.presenting = false
    return self
  }
  
  func interactionControllerForPresentation(animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
    self.presenting = true
    return nil
  }
  
  func interactionControllerForDismissal(animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
    self.presenting = false
    return self
  }
}
