//
//  Friend.swift
//  MoovUpApp
//
//  Created by Gilson Gil on 11/15/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit
import MapKit

class Friend: NSObject {
  
  var location: CLLocation
  var name: String

  init(location: CLLocation, name: String) {
    self.location = location
    self.name = name
    super.init()
  }
}


extension Friend: MKAnnotation {
  var coordinate: CLLocationCoordinate2D {
    return location.coordinate
  }
  var title: String {
    return name
  }
}
