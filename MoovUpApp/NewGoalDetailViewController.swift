//
//  NewGoalDetailViewController.swift
//  MoovUpApp
//
//  Created by Gilson Gil on 11/15/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class NewGoalDetailViewController: UIViewController {
  
  @IBOutlet weak var goalImageView: UIImageView!
  @IBOutlet weak var goalButton: UIButton!
  @IBOutlet weak var nameTextField: UITextField!
  @IBOutlet weak var valueTextField: UITextField!
  @IBOutlet weak var deadlineTextField: UITextField!
  @IBOutlet weak var bgView: UIView!
  @IBOutlet weak var goalImageViewTopSpaceConstraint: NSLayoutConstraint!
  @IBOutlet weak var goalButtonTopSpaceConstraint: NSLayoutConstraint!
  @IBOutlet weak var bgViewBottomSpaceConstraint: NSLayoutConstraint!

  var goal: Goal?
  var selectedOption: (String, String, String)?
  weak var pickerView: PickerView?
  var activeTextField: UITextField?
  var keyboardFrame: CGRect?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    if goal != nil {
      goalImageView.image = UIImage(named: goal!.icon)
    }
    nameTextField.setPaddings()
    nameTextField.setBorders()
    valueTextField.setPaddings()
    valueTextField.setBorders()
    deadlineTextField.setPaddings()
    deadlineTextField.setBorders()
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleKeyboardFrameChange:", name: UIKeyboardDidChangeFrameNotification, object: nil)
  }
  
  @IBAction func toggleGoalOptions(sender: UIButton) {
    if pickerView != nil {
      pickerView!.dismiss()
    } else {
      pickerView = NSBundle.mainBundle().loadNibNamed("PickerView", owner: self, options: nil).first as? PickerView
      pickerView!.frame = CGRect(x: 0, y: view.bounds.height, width: view.bounds.width, height: 206)
      view.layoutIfNeeded()
      pickerView!.presentInViewController(self)
      slideUpGoalControls()
    }
  }
  
  @IBAction func doneTapped(sender: UIButton) {
    let successAlert = UIImageView(image: UIImage(named: "img_confirma_nova_meta"))
    successAlert.alpha = 0.98
    successAlert.contentMode = .ScaleAspectFit
    if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
      successAlert.frame = CGRect(x: (view.bounds.width - 380 - successAlert.bounds.width) / 2, y: (view.bounds.height - successAlert.bounds.height) / 2, width: view.bounds.width - 30, height: successAlert.bounds.height)
    } else {
      successAlert.frame = CGRect(x: (view.bounds.width + 66 - successAlert.bounds.width) / 2, y: (view.bounds.height - successAlert.bounds.height) / 2, width: view.bounds.width - 30, height: successAlert.bounds.height)
    }
    successAlert.hidden = true
    view.addSubview(successAlert)
    successAlert.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0, 0)
    successAlert.hidden = false
    UIView.animateWithDuration(0.3, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1.0, options: .CurveEaseInOut, animations: { () -> Void in
      successAlert.transform = CGAffineTransformIdentity
    }) { (finished) -> Void in
      delay(1.0, { () -> () in
        (UIApplication.sharedApplication().delegate as! AppDelegate).goToTimelineApplicationFlow(self.goal)
      })
    }
  }
  
  func slideUpGoalControls() {
    goalImageViewTopSpaceConstraint.constant = 64 + 14
    goalButtonTopSpaceConstraint.constant = 30
    UIView.animateWithDuration(0.3, animations: { () -> Void in
      self.view.layoutIfNeeded()
    })
  }
  
  func presentBgView() {
    bgViewBottomSpaceConstraint.constant = 0
    bgView.hidden = false
    UIView.animateWithDuration(0.3, animations: { () -> Void in
      self.view.layoutIfNeeded()
    })
  }
  
  func handleKeyboardFrameChange(notification: NSNotification) {
    if let durationValue = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as? NSNumber, let frameValue = notification.userInfo![UIKeyboardFrameEndUserInfoKey] as? NSValue {
      let duration = durationValue.doubleValue
      let frame = frameValue.CGRectValue()
      
      keyboardFrame = frame
      updateKeyboardHandler(frame, duration: duration)
    }
  }
  
  func updateKeyboardHandler(frame: CGRect, duration: Double?) {
    if activeTextField != nil {
      bgViewBottomSpaceConstraint.constant = max(0, frame.height - bgView.bounds.height + activeTextField!.frame.origin.y + activeTextField!.frame.height)
    } else {
      bgViewBottomSpaceConstraint.constant = 0
    }
    UIView.animateWithDuration(duration ?? 0.3, animations: { () -> Void in
      self.view.layoutIfNeeded()
    })
  }
}

extension NewGoalDetailViewController: PickerViewDelegate {
  
  func doneWithIndex(index: Int) {
    goal!.selectedOption = index
    selectedOption = goal!.options[index]
    goalButton.setTitle(selectedOption!.0, forState: .Normal)
    valueTextField.placeholder = selectedOption!.1
    presentBgView()
  }
}

extension NewGoalDetailViewController: UITextFieldDelegate {
  
  func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
    activeTextField = textField
    return true
  }
  
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    if textField == nameTextField {
      valueTextField.becomeFirstResponder()
      activeTextField = valueTextField
    } else if textField == valueTextField {
      deadlineTextField.becomeFirstResponder()
      activeTextField = deadlineTextField
    } else {
      deadlineTextField.resignFirstResponder()
      activeTextField = nil
    }
    if keyboardFrame != nil {
      updateKeyboardHandler(keyboardFrame!, duration: nil)
    }
    return true
  }
}
