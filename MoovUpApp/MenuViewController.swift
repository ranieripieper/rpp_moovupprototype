//
//  MenuViewController.swift
//  MoovUpApp
//
//  Created by Gilson Gil on 11/17/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {
  
  @IBOutlet weak var tap: UITapGestureRecognizer!
  @IBOutlet weak var menuClosePanGesture: UIPanGestureRecognizer!
  
  weak var timelineNavigationController: TimelineNavigationController!

  override func viewDidLoad() {
    super.viewDidLoad()
    view.addGestureRecognizer(tap)
    view.addGestureRecognizer(menuClosePanGesture)
  }
  
  @IBAction func didTap(sender: UITapGestureRecognizer) {
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  @IBAction func menuClosePanGestureRecognizer(menuPanGestureRecognizer: UIPanGestureRecognizer) {
    if menuPanGestureRecognizer.state == .Began {
      let menuAnimator = transitioningDelegate as! MenuAnimator
      menuAnimator.interactive = true
      dismissViewControllerAnimated(true, completion: nil)
    } else if menuPanGestureRecognizer.state == .Changed {
      let translation = menuPanGestureRecognizer.translationInView(view)
      let d = max(0, translation.x / CGRectGetWidth(view.bounds))
      let menuAnimator = transitioningDelegate as! MenuAnimator
      menuAnimator.updateInteractiveTransition(d)
    } else if menuPanGestureRecognizer.state == .Ended {
      let menuAnimator = transitioningDelegate as! MenuAnimator
      if menuPanGestureRecognizer.velocityInView(view).x > 0 {
        menuAnimator.finishInteractiveTransition()
      } else {
        menuAnimator.cancelInteractiveTransition()
      }
      menuAnimator.interactive = false
    }
  }
  
  @IBAction func myGoalsTapped(sender: UIButton) {
    dismissViewControllerAnimated(true, completion: nil)
    timelineNavigationController.showTimelineViewController()
  }
  
  @IBAction func newGoalTapped(sender: UIButton) {
    dismissViewControllerAnimated(true, completion: nil)
    (UIApplication.sharedApplication().delegate as! AppDelegate).goToNewGoalApplicationFlowFromLogin(false)
  }
  
  @IBAction func askSpecialistTapped(sender: UIButton) {
    dismissViewControllerAnimated(true, completion: nil)
    timelineNavigationController.showAskSpecialistViewController()
  }
}
