//
//  LoginViewController.swift
//  MoovUpApp
//
//  Created by Gilson Gil on 11/15/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit
import MapKit

class LoginViewController: UIViewController {
  
  @IBOutlet weak var bgView: UIView!
  @IBOutlet weak var mapView: MKMapView!
  @IBOutlet weak var bgViewBottomSpaceConstraint: NSLayoutConstraint!
  @IBOutlet weak var googlePlusButtonHorizontalCenterConstraint: NSLayoutConstraint!
  
  var loginFieldsHidden = true
  var hasPlacedPins = false
  var locationManager = LocationManager()
  var friends = [Friend]()

  override func viewDidLoad() {
    super.viewDidLoad()
    locationManager.locationManagerDelegate = self
    navigationController?.navigationBarHidden = true
  }
  
  override func preferredStatusBarStyle() -> UIStatusBarStyle {
    return .Default
  }
  
  @IBAction func facebookTapped(sender: UIButton) {
    (UIApplication.sharedApplication().delegate as! AppDelegate).goToNewGoalApplicationFlowFromLogin(true)
  }
  
  @IBAction func googlePlusTapped(sender: UIButton) {
    (UIApplication.sharedApplication().delegate as! AppDelegate).goToNewGoalApplicationFlowFromLogin(true)
  }
  
  @IBAction func createAccountTapped(sender: UIButton) {
    if IOS_8() {
      let alert = UIAlertController(title: "MOOVUP", message: "Função não implementada", preferredStyle: .Alert)
      alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
      presentViewController(alert, animated: true, completion: nil)
    } else {
      let alert = UIAlertView(title: "MOOVUP", message: "Função não implementada", delegate: nil, cancelButtonTitle: "Ok")
      alert.show()
    }
  }
  
  func addPinsToMap() {
    hasPlacedPins = true
    friends = [
      Friend(location: CLLocation(latitude: locationManager.userLocation!.coordinate.latitude + 0.002, longitude: locationManager.userLocation!.coordinate.longitude + 0.001), name: "1"),
      Friend(location: CLLocation(latitude: locationManager.userLocation!.coordinate.latitude - 0.002, longitude: locationManager.userLocation!.coordinate.longitude + 0.002), name: "2"),
      Friend(location: CLLocation(latitude: locationManager.userLocation!.coordinate.latitude - 0.001, longitude: locationManager.userLocation!.coordinate.longitude + 0.005), name: "3"),
      Friend(location: CLLocation(latitude: locationManager.userLocation!.coordinate.latitude - 0.0035, longitude: locationManager.userLocation!.coordinate.longitude - 0.0045), name: "4"),
      Friend(location: CLLocation(latitude: locationManager.userLocation!.coordinate.latitude - 0.0045, longitude: locationManager.userLocation!.coordinate.longitude - 0.0035), name: "5"),
      Friend(location: CLLocation(latitude: locationManager.userLocation!.coordinate.latitude + 0.0035, longitude: locationManager.userLocation!.coordinate.longitude + 0.0055), name: "6"),
      Friend(location: CLLocation(latitude: locationManager.userLocation!.coordinate.latitude + 0.0023, longitude: locationManager.userLocation!.coordinate.longitude + 0.0063), name: "7"),
      Friend(location: CLLocation(latitude: locationManager.userLocation!.coordinate.latitude + 0.003, longitude: locationManager.userLocation!.coordinate.longitude - 0.007), name: "8"),
      Friend(location: CLLocation(latitude: locationManager.userLocation!.coordinate.latitude - 0.001, longitude: locationManager.userLocation!.coordinate.longitude - 0.0015), name: "9"),
      Friend(location: CLLocation(latitude: locationManager.userLocation!.coordinate.latitude + 0.0015, longitude: locationManager.userLocation!.coordinate.longitude - 0.004), name: "10"),
      Friend(location: CLLocation(latitude: locationManager.userLocation!.coordinate.latitude + 0.0045, longitude: locationManager.userLocation!.coordinate.longitude), name: "11")
    ]
    for friend in friends {
      mapView.addAnnotation(friend)
    }
    let span = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
    let region = MKCoordinateRegion(center: locationManager.userLocation!.coordinate, span: span)
    mapView.setRegion(region, animated: true)
  }
}

extension LoginViewController: MKMapViewDelegate {
  
  func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
    if let friend = annotation as? Friend {
      var annotationView = mapView.dequeueReusableAnnotationViewWithIdentifier("Friend")
      if annotationView == nil {
        annotationView = MKAnnotationView (annotation: friend, reuseIdentifier: "Friend")
        annotationView.enabled = true
        annotationView.image = UIImage (named: "img_pin\(friend.name)")
      } else {
        annotationView.annotation = friend
      }
      return annotationView
    }
    return nil
  }
}

extension LoginViewController: LocationManagerDelegate {
  
  func didUpdateLocation(userLocation: CLLocation) {
    if !hasPlacedPins {
      addPinsToMap()
    }
  }
}
