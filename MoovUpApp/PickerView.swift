//
//  PickerView.swift
//  MoovUpApp
//
//  Created by Gilson Gil on 11/16/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

@objc protocol PickerViewDelegate {
  func doneWithIndex(index: Int)
}

class PickerView: UIView {
  
  @IBOutlet weak var pickerView: UIPickerView!
  
  var datasource: [String]?
  weak var delegate: PickerViewDelegate?

  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }

  @IBAction func done(sender: UIButton) {
    delegate?.doneWithIndex(pickerView.selectedRowInComponent(0))
    dismiss()
  }
  
  func presentInViewController(viewController: UIViewController) {
    if let newGoalDetailViewController = viewController as? NewGoalDetailViewController {
      datasource = newGoalDetailViewController.goal!.options.map({ (option: (String, String, String)) -> String in
        return option.0
      })
      delegate = newGoalDetailViewController
    } else if let askSpecialistViewController = viewController as? AskSpecialistViewController {
      datasource = ["Saúde", "Exercício Físico", "Conquistas Pessoais", "Mente", "Viagens"]
      delegate = askSpecialistViewController
    }
    hidden = false
    viewController.view.addSubview(self)
    UIView.animateWithDuration(0.3, animations: { () -> Void in
      self.frame = CGRect(x: 0, y: self.frame.origin.y - self.frame.height, width: self.frame.width, height: self.frame.height)
    })
  }
  
  func dismiss() {
    delegate = nil
    UIView.animateWithDuration(0.3, animations: { () -> Void in
      self.frame = CGRect(x: 0, y: self.frame.origin.y + self.frame.height, width: self.frame.width, height: self.frame.height)
    }) { (finished) -> Void in
      self.hidden = true
      self.removeFromSuperview()
    }
  }
}

extension PickerView: UIPickerViewDataSource, UIPickerViewDelegate {
  
  func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
    return 1
  }
  
  func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return datasource?.count ?? 0
  }
  
  func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView!) -> UIView {
    let frame = CGRect(x: 0, y: 0, width: self.frame.width, height: 44)
    let label = UILabel(frame: frame)
    label.text = datasource?[row]
    label.textAlignment = .Left
    label.font = UIFont.systemFontOfSize(20)
    return label
  }
  
  func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    
  }
}
