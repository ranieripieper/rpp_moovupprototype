//
//  LocationManager.swift
//  MoovUpApp
//
//  Created by Gilson Gil on 11/15/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit
import MapKit

@objc protocol LocationManagerDelegate {
  optional func didUpdateLocation(userLocation: CLLocation)
  optional func didRespondToRequestWithStatus(status: CLAuthorizationStatus)
}

class LocationManager: CLLocationManager {
  
  var userLocation: CLLocation?
  weak var locationManagerDelegate: LocationManagerDelegate?
  
  override init() {
    super.init()
    delegate = self
    desiredAccuracy = kCLLocationAccuracyBest
    if IOS_8() {
      requestWhenInUseAuthorization()
    } else {
      startUpdatingLocation()
    }
  }
  
  class func isAuthorized() -> Bool {
    return authorizationStatus() == .AuthorizedWhenInUse
  }
}

extension LocationManager: CLLocationManagerDelegate {
  
  func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
    if status == .AuthorizedWhenInUse {
      startUpdatingLocation()
    }
  }
  
  func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
    if let location = locations.last as? CLLocation {
      if userLocation == nil || location.distanceFromLocation(userLocation) > 100 {
        userLocation = location
        locationManagerDelegate?.didUpdateLocation?(userLocation!)
      }
    }
  }
  
  func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
    
  }
}
