//
//  LoginNavigationControllerDelegate.swift
//  MoovUpApp
//
//  Created by Gilson Gil on 11/17/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class LoginNavigationControllerDelegate: NSObject {
  
  @IBOutlet weak var navigationController: UINavigationController!
  
  let animator = LoginAnimator()
  var interactionController: UIPercentDrivenInteractiveTransition?
  
  override func awakeFromNib() {
    let popRecognizer = UIPanGestureRecognizer (target: self, action: "popGestureRecognizer:")
    navigationController.view.addGestureRecognizer(popRecognizer)
  }
  
  func popGestureRecognizer (popGestureRecognizer: UIPanGestureRecognizer) {
    let view = self.navigationController.view
    if popGestureRecognizer.state == .Began {
      self.interactionController = UIPercentDrivenInteractiveTransition ()
      navigationController.popViewControllerAnimated(true)
    } else if popGestureRecognizer.state == .Changed {
      let translation = popGestureRecognizer.translationInView(view)
      let d = fabs(translation.x / CGRectGetWidth(view.bounds))
      interactionController?.updateInteractiveTransition(d)
    } else if popGestureRecognizer.state == .Ended {
      if popGestureRecognizer.velocityInView(view).x > 0 {
        interactionController?.finishInteractiveTransition()
      } else {
        interactionController?.cancelInteractiveTransition()
      }
      interactionController = nil
    }
  }
}

extension LoginNavigationControllerDelegate: UINavigationControllerDelegate {
  
  func navigationController(navigationController: UINavigationController, animationControllerForOperation operation: UINavigationControllerOperation, fromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    if operation == .Pop {
      animator.presenting = false
    } else {
      animator.presenting = true
    }
    return animator
  }
  
  func navigationController(navigationController: UINavigationController, interactionControllerForAnimationController animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
    return interactionController
  }
}
