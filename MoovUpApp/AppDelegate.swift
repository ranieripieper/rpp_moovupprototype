//
//  AppDelegate.swift
//  MoovUpApp
//
//  Created by Gilson Gil on 11/14/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
    return true
  }
  
  func goToNewGoalApplicationFlowFromLogin(fromLogin: Bool) {
    if window != nil {
      let rootViewController = UIStoryboard(name: "NewGoal", bundle: NSBundle.mainBundle()).instantiateInitialViewController() as? UIViewController
      if fromLogin {
        rootViewController!.view.frame = CGRect(x: window!.bounds.width, y: 0, width: rootViewController!.view.bounds.width, height: rootViewController!.view.bounds.height)
        window!.addSubview(rootViewController!.view)
        UIView.animateWithDuration(0.3, delay: 0.0, options: .CurveEaseInOut, animations: { () -> Void in
          rootViewController!.view.frame = self.window!.bounds
        }, completion: { (finished) -> Void in
          self.window!.rootViewController = rootViewController
        })
      } else {
        let oldRootViewController = window!.rootViewController
        rootViewController!.view.frame = window!.bounds
        window!.insertSubview(rootViewController!.view, belowSubview: oldRootViewController!.view)
        UIView.animateWithDuration(0.5, delay: 0.0, options: .CurveEaseInOut, animations: { () -> Void in
          oldRootViewController!.view.frame = CGRect(x: self.window!.bounds.width, y: 0, width: self.window!.bounds.width, height: self.window!.bounds.height)
        }, completion: { (finished) -> Void in
          self.window!.rootViewController = rootViewController
        })
      }
    }
  }
  
  func goToTimelineApplicationFlow(goal: Goal?) {
    if window != nil {
      let navigationController = UIStoryboard(name: "Timeline", bundle: NSBundle.mainBundle()).instantiateInitialViewController() as? UINavigationController
      let timelineViewController = navigationController!.viewControllers.first as! TimelineViewController
      timelineViewController.goal = goal
      navigationController!.view.frame = CGRect(x: window!.bounds.width, y: 0, width: navigationController!.view.bounds.width, height: navigationController!.view.bounds.height)
      self.window!.addSubview(navigationController!.view)
      UIView.animateWithDuration(0.3, delay: 0.0, options: UIViewAnimationOptions.CurveEaseInOut, animations: { () -> Void in
        navigationController!.view.frame = self.window!.bounds
      }, completion: { (finished) -> Void in
        self.window!.rootViewController = navigationController
      })
    }
  }
  
  func setAppearance() {
    let appearanceNavigationBar = UINavigationBar.appearance()
    appearanceNavigationBar.backIndicatorImage = UIImage (named: "btn_back")
    appearanceNavigationBar.backIndicatorTransitionMaskImage = UIImage (named: "btn_back")
  }
}

