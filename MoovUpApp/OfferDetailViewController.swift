//
//  OfferDetailViewController.swift
//  MoovUpApp
//
//  Created by Gilson Gil on 11/17/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class OfferDetailViewController: UIViewController {
  
  @IBOutlet weak var sponsorImageView: UIImageView!
  
  var sponsor: String?

  override func viewDidLoad() {
    super.viewDidLoad()
    if sponsor != nil {
      sponsorImageView.image = UIImage(named: "img_detalhe_patrocinador_\(sponsor!)")
    }
  }
  
  @IBAction func showMenu(sender: UIBarButtonItem) {
    navigationController?.performSegueWithIdentifier("SegueMenu", sender: nil)
  }
}
