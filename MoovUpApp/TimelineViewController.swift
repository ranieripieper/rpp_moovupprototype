//
//  TimelineViewController.swift
//  MoovUpApp
//
//  Created by Gilson Gil on 11/17/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class TimelineViewController: UIViewController {
  
  @IBOutlet weak var timelineImageView: UIImageView!
  
  var goal: Goal?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    if goal != nil {
      timelineImageView.image = UIImage(named: goal!.timelineImage)
    }
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if let offerDetailViewController = segue.destinationViewController as? OfferDetailViewController {
      offerDetailViewController.sponsor = sender as? String
    }
    navigationItem.backBarButtonItem = UIBarButtonItem (title: "", style: .Bordered, target: nil, action: nil)
  }
  
  @IBAction func goToOffer(sender: UIButton) {
    let sponsor = goal!.sponsors[sender.tag - 1]
    performSegueWithIdentifier("SegueOfferDetail", sender: sponsor)
  }
  
  @IBAction func showMenu(sender: UIBarButtonItem) {
    navigationController?.performSegueWithIdentifier("SegueMenu", sender: sender)
  }
  
  @IBAction func newPostTapped(sender: UIButton) {
    let newPostNavigationController = UIStoryboard(name: "NewPost", bundle: NSBundle.mainBundle()).instantiateInitialViewController() as! UINavigationController
    let newPostViewController = newPostNavigationController.viewControllers.first as! NewPostViewController
    newPostViewController.goal = goal
    presentViewController(newPostNavigationController, animated: true, completion: nil)
  }
  
  @IBAction func newPostTouchedDown(sender: UIButton) {
    UIView.animateWithDuration(0.3, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 1.0, options: .AllowUserInteraction, animations: { () -> Void in
      sender.layer.transform = CATransform3DScale(CATransform3DIdentity, 0.8, 0.8, 0.8)
    }, completion: nil)
  }
  
  @IBAction func newPostTouchedUp(sender: UIButton) {
    UIView.animateWithDuration(0.3, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 1.0, options: .AllowUserInteraction, animations: { () -> Void in
      sender.layer.transform = CATransform3DIdentity
    }, completion: nil)
  }
  
  @IBAction func slapTapped(sender: UIButton) {
    sender.selected = !sender.selected
  }
}
