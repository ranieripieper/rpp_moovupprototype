//
//  Goal.swift
//  MoovUpApp
//
//  Created by Gilson Gil on 11/16/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class Goal: NSObject {
  
  var type: Int
  var options: [(String, String, String)]
  var icon: String
  var timelineImage: String
  var sponsors: [String]
  var selectedOption: Int?
  
  init(type: Int) {
    self.type = type
    let goalsArray = Goal.goalsArray()
    let index = type - 1
    let goalDict = goalsArray[index]
    let imageName = goalDict["ImageName"] as? String ?? ""
    self.icon = "icn_input_meta_\(imageName)"
    self.timelineImage = "img_home_\(imageName)"
    let optionsDict = goalDict["Options"] as? [[String: String]] ?? []
    self.options = []
    for optionDict in optionsDict {
      let name = optionDict["Name"]
      let placeholder = optionDict["Placeholder"]
      let postPlaceholder = optionDict["Post-Placeholder"]
      if name != nil && placeholder != nil {
        let tuple = (name!, placeholder!, postPlaceholder!) as (String, String, String)
        self.options.append(tuple)
      }
    }
    let sponsors = goalDict["Sponsors"] as? [String] ?? []
    self.sponsors = []
    for sponsor in sponsors {
      self.sponsors.append(sponsor)
    }
    super.init()
  }
  
  class func goalsArray() -> [[String: AnyObject]] {
    let goalsArray = [
      ["Name": "Health", "ImageName": "saude", "Options": [["Name": "Perder peso", "Placeholder": "Quantos kg?", "Post-Placeholder": "Quantos kg você perdeu?"], ["Name": "Trabalhar menos", "Placeholder": "Quantas horas por semana?", "Post-Placeholder": "Quantos horas por semana você fez?"], ["Name": "Outros", "Placeholder": "Qual a quantidade?", "Post-Placeholder": "Qual a quantidade feita?"]], "Sponsors": ["mundoverde", "cvc"]],
    ["Name": "Exercise", "ImageName": "exercicios", "Options": [["Name": "Correr", "Placeholder": "Quantos km?", "Post-Placeholder": "Quantos km você correu?"], ["Name": "Andar de bicicleta", "Placeholder": "Quantos km?", "Post-Placeholder": "Quantos km você pedalou?"], ["Name": "Escalada", "Placeholder": "Qual altitude?", "Post-Placeholder": "Qual altitude você atingiu?"], ["Name": "Surfing", "Placeholder": "Quantas vezes por mês?", "Post-Placeholder": "Quantas vezes você foi?"], ["Name": "Outros", "Placeholder": "Qual a quantidade?", "Post-Placeholder": "Qual a quantidade feita?"]], "Sponsors": ["nike", "tracknfield"]],
    ["Name": "Personal", "ImageName": "pessoais", "Options": [["Name": "Comprar algo", "Placeholder": "Qual o valor?", "Post-Placeholder": "Comprou o quê?"], ["Name": "Ser promovido", "Placeholder": "Qual o novo salário?", "Post-Placeholder": "Qual seu salário agora?"], ["Name": "Outros", "Placeholder": "Qual a quantidade?", "Post-Placeholder": "Qual a quantidade feita?"]], "Sponsors": ["ford", "tokstock"]],
    ["Name": "Mental", "ImageName": "mente", "Options": [["Name": "Completar um quebra-cabeça", "Placeholder": "Quantas peças?", "Post-Placeholder": "Quantas peças você montou?"], ["Name": "Fazer um MBA", "Placeholder": "Quantos anos?", "Post-Placeholder": "Quantos anos você cursou?"], ["Name": "Ler mais livros", "Placeholder": "Quantos livros?", "Post-Placeholder": "Quantos livros você leu?"], ["Name": "Outros", "Placeholder": "Qual a quantidade?", "Post-Placeholder": "Qual a quantidade feita?"]], "Sponsors": ["estrela", "lego"]],
    ["Name": "Travelling", "ImageName": "viagens", "Options": [["Name": "Viajar para algum lugar", "Placeholder": "Quantos dias?", "Post-Placeholder": "Por quantos dias você viajou?"], ["Name": "Aumentar o número de viagens por ano", "Placeholder": "Quantas viagens?", "Post-Placeholder": "Quantas viagens você fez?"], ["Name": "Outros", "Placeholder": "Qual a quantidade?", "Post-Placeholder": "Qual a quantidade feita?"]], "Sponsors": ["cvc", "tam"]]] as [[String: AnyObject]]
    return goalsArray
//    let path = NSBundle.mainBundle().pathForResource("Goals", ofType: "plist")
//    let goalsArray = NSArray(contentsOfFile: path!)
//    return goalsArray as [[String: AnyObject]]
  }
}
