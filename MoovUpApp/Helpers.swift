//
//  Helpers.swift
//  MoovUpApp
//
//  Created by Gilson Gil on 11/15/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

extension UITextField {
  
  func setPaddings() {
    let leftPadding = UIView(frame: CGRect(origin: CGPointZero, size: CGSize(width: 20, height: frame.height)))
    let rightPadding = UIView(frame: CGRect(origin: CGPointZero, size: CGSize(width: 20, height: frame.height)))
    leftView = leftPadding
    leftViewMode = .Always
    rightView = rightPadding
    rightViewMode = .Always
  }
  
  func setBorders() {
    layer.borderColor = UIColor(red: 220.0/255, green: 220.0/255, blue: 220.0/255, alpha: 1.0).CGColor
    layer.borderWidth = 1.0
  }
  
  func setLightPlaceholder(placeholder: String) {
    if respondsToSelector("setAttributedPlaceholder:") {
      attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [NSForegroundColorAttributeName: UIColor(red: 203.0/255, green: 203.0/255, blue: 203.0/255, alpha: 1.0)])
    }
  }
}

extension UITextView {
  
  func setPaddings() {
    textContainerInset = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
  }
  
  func setBorders() {
    layer.borderColor = UIColor(red: 220.0/255, green: 220.0/255, blue: 220.0/255, alpha: 1.0).CGColor
    layer.borderWidth = 1.0
  }
}

func delay(delay:Double, closure:()->()) {
  dispatch_after(
    dispatch_time(
      DISPATCH_TIME_NOW,
      Int64(delay * Double(NSEC_PER_SEC))
    ),
    dispatch_get_main_queue(), closure)
}

func IOS_8() -> Bool {
  switch UIDevice.currentDevice().systemVersion.compare("8.0.0", options: NSStringCompareOptions.NumericSearch) {
  case .OrderedSame, .OrderedDescending:
    return true
  case .OrderedAscending:
    return false
  }
}
